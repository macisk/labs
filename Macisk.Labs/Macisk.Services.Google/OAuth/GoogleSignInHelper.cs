﻿using Macisk.Services.Utility.ApiHelpers;
using Macisk.Services.Utility.OAuthHelpers;
using Microsoft.AspNet.Identity;
using Microsoft.Owin;
using Microsoft.Owin.Security.Google;
using Owin;
using System;
using System.Configuration;
using System.Net.Http;

namespace Macisk.Services.Google.OAuth
{
    public static class GoogleSignInHelper
    {
        #region For Server
        public static string ExternalAuthenticationType = "External";
        public static string GoogleAuthenticationType = "Google";
        public static string GoogleClientId = ConfigurationManager.AppSettings["Google.ClientId"];
        public static string GoogleClientSecret = ConfigurationManager.AppSettings["Google.ClientSecret"];


        public static void InitailGoogleSignIn(IAppBuilder app)
        {

            app.UseExternalSignInCookie(DefaultAuthenticationTypes.ExternalCookie);
            //Google外部登入使用者
            var google = new GoogleOAuth2AuthenticationOptions
            {
                AuthenticationType = GoogleSignInHelper.GoogleAuthenticationType,
                ClientId = GoogleSignInHelper.GoogleClientId,
                ClientSecret = GoogleSignInHelper.GoogleClientSecret,
                CallbackPath = new PathString("/oauth/callback")

            };
            google.Scope.Add("openid");
            google.Scope.Add("profile");
            google.Scope.Add("email");
            app.UseGoogleAuthentication(google);

        }
        #endregion

        #region For Client 
        public static string ExternalIdentifyApi = ConfigurationManager.AppSettings["ExternalIdentifyApi"];
        

        public static ProviderUser GetExternalUser(string Identify)
        {
            using (HttpClient Client = new HttpClient())
            {
                try
                {
                    return Client.ApiGet<ProviderUser>($"{ExternalIdentifyApi}?Identify={Identify}");
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
        }

        #endregion

    }
    

}