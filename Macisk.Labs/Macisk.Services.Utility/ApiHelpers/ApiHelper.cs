﻿using Newtonsoft.Json;
using System.Net.Http;
using System.Text;

namespace Macisk.Services.Utility.ApiHelpers
{

    public static class ApiHelper
    {

        public static string BuildArrayQueryByName(this HttpClient ClientThis, string QueryName, string[] QueryValues)
        {
            var Query = string.Empty;
            foreach (var Val in QueryValues)
                Query += string.Format("{2}{0}={1}", QueryName, Val, (Query == string.Empty ? string.Empty : "&"));
            return Query;
        }

        public static TResult ApiGet<TResult>(this HttpClient ClientThis, string ApiQuery)
        {
            //TryAppendCulture(ClientThis);
            var Result = ClientThis.GetAsync(ApiQuery).Result.Content.ReadAsStringAsync().Result;
            return JsonConvert.DeserializeObject<TResult>(Result);
        }

        public static TResult ApiPost<TInput, TResult>(this HttpClient ClientThis, string ApiQuery, TInput Data)
        {
            ClientThis.DefaultRequestHeaders.Accept.Clear();
            ClientThis.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
            //TryAppendCulture(ClientThis);
            var Result = ClientThis.PostAsync(ApiQuery,
                new StringContent(JsonConvert.SerializeObject(Data), Encoding.UTF8, "application/json")
                ).Result.Content.ReadAsStringAsync().Result;
            return JsonConvert.DeserializeObject<TResult>(Result);
        }

        public static TResult ApiPut<TInput, TResult>(this HttpClient ClientThis, string ApiQuery, TInput Data)
        {
            ClientThis.DefaultRequestHeaders.Accept.Clear();
            ClientThis.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
            //TryAppendCulture(ClientThis);
            var Result = ClientThis.PutAsync(ApiQuery,
                new StringContent(JsonConvert.SerializeObject(Data), Encoding.UTF8, "application/json")
                ).Result.Content.ReadAsStringAsync().Result;
            return JsonConvert.DeserializeObject<TResult>(Result);
        }

        public static TResult ApiDelete<TResult>(this HttpClient ClientThis, string ApiQuery)
        {
            //TryAppendCulture(ClientThis);
            var Result = ClientThis.DeleteAsync(ApiQuery).Result.Content.ReadAsStringAsync().Result;
            return JsonConvert.DeserializeObject<TResult>(Result);
        }




    }
}