﻿
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace Macisk.Services.Utility.OAuthHelpers
{
    public class ReplayHelper
    {
        
        public static Dictionary<string, string> ReplayList { get; set; }

        public static string SetReplayUrl(string SessionId , string ReplayUrl)
        {
            if (ReplayList == null) ReplayList = new Dictionary<string, string>();
            var Key = Convert.ToBase64String(Encoding.UTF8.GetBytes($"{SessionId}@{ReplayUrl}"));
            if (ReplayList.ContainsKey(Key))
                ReplayList[Key] = ReplayUrl;
            else
                ReplayList.Add(Key, ReplayUrl);
            return Key;

        }

        public static string GetReplayUrl(string Key)
        {
            if (string.IsNullOrEmpty(Key)) return string.Empty;
            if (!ReplayList.ContainsKey(Key)) return string.Empty;
            var ReplayUrl = ReplayList[Key];
            ReplayList.Remove(Key);
            return ReplayUrl;
        }

        public static Dictionary<string, ProviderUser> IdentifyUserList { get; set; }

        public static string SetIdentifyUser(ExternalLoginInfo ExternalInfo)
        {
            if (IdentifyUserList == null) IdentifyUserList = new Dictionary<string, ProviderUser>();
            if (ExternalInfo == null) return string.Empty;
            var ProviderUser = new ProviderUser {
                Name = ExternalInfo.DefaultUserName , 
                Email = ExternalInfo.Email ,
                Provider = ExternalInfo.Login.LoginProvider , 
                ProviderKey = ExternalInfo.Login.ProviderKey 
            };
            var Identify = Convert.ToBase64String(Encoding.UTF8.GetBytes($"{ProviderUser.ProviderKey}@{ProviderUser.Provider}"));
            if (IdentifyUserList.ContainsKey(Identify))
                IdentifyUserList[Identify] = ProviderUser;             
            else
                IdentifyUserList.Add(Identify, ProviderUser);
            return Identify;
        }

        public static ProviderUser GetIdentifyUser(string Identify)
        {
            if (string.IsNullOrEmpty(Identify)) return null;
            if (!IdentifyUserList.ContainsKey(Identify)) return null;
            return IdentifyUserList[Identify];            
        }

    }

    public class ProviderUser
    {
        public string Provider { get; set; }

        public string ProviderKey { get; set; }

        public string Name { get; set; }

        public string Email { get; set; }

    }

}