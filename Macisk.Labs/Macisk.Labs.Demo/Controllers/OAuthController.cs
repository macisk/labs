﻿using Macisk.Services.Google.OAuth;
using Macisk.Services.Utility.OAuthHelpers;
using Microsoft.Owin.Security;
using System.Web;
using System.Web.Mvc;

namespace Macisk.Labs.Demo.Controllers
{
    public class OAuthController : Controller
    {        



        [AllowAnonymous]
        // GET: OAuth
        public ActionResult Binding(string Provider,string ReplayUrl)
        {
            var ReplayKey = ReplayHelper.SetReplayUrl(Session.SessionID, ReplayUrl);
            return new ChallengeResult("Google", $"Replay?ReplayKey={ReplayKey}");
        }
        
        // GET: OAuth
        public ActionResult Replay(string ReplayKey)
        {
            IAuthenticationManager Manager = ((IAuthenticationManager)HttpContext.GetOwinContext().Authentication);
            var ProviderKey = Manager.GetExternalLoginInfo().Login.ProviderKey;
            var ReplayUrl = Server.UrlDecode($"{ReplayHelper.GetReplayUrl(ReplayKey)}");
            var Identify = ReplayHelper.SetIdentifyUser(Manager.GetExternalLoginInfo());
            if (ReplayUrl.Contains("?"))
                ReplayUrl += $"&Identify={Identify}";
            else
                ReplayUrl += $"?Identify={Identify}";
            return Redirect(ReplayUrl);
        }

        public ActionResult Identify(string Identify)
        {
            ViewBag.ProviderKey = Identify;
            return View();
        }


        public ActionResult Start()
        {
            return View();
        }

        public ActionResult End(string Identify)
        {
            var User = GoogleSignInHelper.GetExternalUser(Identify);
            if (User == null) return RedirectToAction("start");
            ViewBag.Name = User.Name;
            ViewBag.Email = User.Email;
            ViewBag.Provider = User.Provider;
            ViewBag.ProviderKey = User.ProviderKey;
            return View();
        }
    }
    

}