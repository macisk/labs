﻿using Macisk.Services.Utility.OAuthHelpers;
using System;
using System.Web.Http;

namespace Macisk.Labs.Demo.Controllers
{
    public class IdentifyController : ApiController
    {
        [Route("oauth/identify"), HttpGet]
        public IHttpActionResult GetIdentifyUser(string Identify)
        {

            try
            {
                return Ok(ReplayHelper.GetIdentifyUser(Identify));
            }
            catch (Exception Ex)
            {
                return InternalServerError();
            }
        }

    }
}
