﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Macisk.Services.Google.OAuth;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Owin;

namespace Macisk.Labs.Demo
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {

            #region 外部登入

            //啟用外部登入
            GoogleSignInHelper.InitailGoogleSignIn(app);

            #endregion

        }

    }
}